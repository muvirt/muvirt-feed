'use strict';
'require baseclass';
'require rpc';

var callQMIOverview = rpc.declare({
	'object': 'qmi',
	'method': 'overview'
});

var callHwInfo = rpc.declare({
	'object': 'qmi',
	'method': 'hwinfo'
});

return baseclass.extend({
	title: _('Cellular'),
	load: function() {
		return Promise.all([
			L.resolveDefault(callQMIOverview(), {}),
			L.resolveDefault(callHwInfo(), {}),
		])
	},
	render: function(data) {
		var celloverview = data[0];
		var hwinfo = data[1];
		//var cellstatus = E('dev', { 'class': 'network-status-table' });
		const fields = new Map();
		if ("state" in celloverview) {
			fields.set(_("Status"),celloverview["state"]);
		}
		if ("network_name" in celloverview) {
			fields.set(_("Network"),celloverview["network_name"]);
		}
		if ("mode" in celloverview) {
			fields.set(_("Mode"),celloverview["mode"]);
		}
		if ("bars" in celloverview) {
			const bars = celloverview["bars"];
			var icon;
			switch(bars) {
				case 5:
					icon = L.resource("icons/signal-75-100.png");
					break;
				case 4:
					icon = L.resource("icons/signal-50-75.png");
					break;
				case 3:
					icon = L.resource("icons/signal-25-50.png");
					break;
				case 2:
					icon = L.resource("icons/signal-0-25.png");
					break;
				default:
					icon = L.resource("icons'signal-0.png");
			}
			var signal = ("lte_rssi" in celloverview) ? celloverview["lte_rssi"] + " dBm " : "";
			var signal_span = E('span', [E('span', [signal]), E('img', {'src': icon})]);
			fields.set(_("Signal (RSSI)"), signal_span);
		}
		if ("imei" in hwinfo) {
			fields.set(_("IMEI"),hwinfo["imei"]);
		}
		if ("active_sim_slot" in celloverview) {
			fields.set(_("Active SIM Slot"), celloverview["active_sim_slot"]);
		}
		var table = E('table', { 'class': 'table' });
		
		for (const [key,value] of fields) {
			table.appendChild(E('tr', { 'class': 'tr' }, [
				E('td', { 'class': 'td left', 'width': '33%' }, [ key ]),
				E('td', { 'class': 'td left' }, [ value ])
			]));
		}
	
		return table;
	}
});