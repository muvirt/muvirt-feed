'use strict';
'require rpc';
'require view';
'require poll';
'require ui';

var callSFPStatus = rpc.declare({
	'object': 'sfp',
	'method': 'status',
});

return view.extend({
	load: function() {
		return callSFPStatus();
	},
	renderOverviewTabTable(data) {
		var module_info = data["ethtool"];
		var module_status = data["status"];
		const fields = new Map();
		if (typeof(module_status) == "object") {
			if ("present" in module_status) {
				let present_status = module_status["present"];
				let present_text = present_status ? _("Yes") : _("No");
				fields.set(_("Module present"), present_text);
			}
			if ("rxlos" in module_status) {
				let rxlos_status = module_status["rxlos"];
				let rxlos_text = rxlos_status ? _("Active") : _("Inactive");
				fields.set(_("Loss of Signal (LOS) alarm"), rxlos_text);
			}
			if ("txfault" in module_status) {
				let txfault_status = module_status["txfault"];
				let txstatus = txfault_status ? _("Fault") : _("Inactive");
				fields.set(_("Transmit Fault (TXFAULT) alarm"), txstatus);
			}
			if ("link_up" in module_status) {
				let linkup_text = module_status["link_up"] ? "Up" : "Down";
				fields.set(_("Link status"), linkup_text);
			}
		}
		if (typeof(module_info) == "object") {
			if ("vendor_name" in module_info) {
				fields.set(_("Vendor"),module_info["vendor_name"]);
			}
			if ("vendor_pn" in module_info) {
				var vendor_part_string = module_info["vendor_pn"];
				if ("vendor_rev" in module_info &&
					module_info["vendor_rev"].length > 0) {
					vendor_part_string = vendor_part_string + " " +
						_("Revision") + " " +  module_info["vendor_rev"];
					fields.set(_("Vendor Part"),vendor_part_string);
				}
			}
			if ("vendor_sn" in module_info) {
				fields.set(_("Transceiver Serial Number"), module_info["vendor_sn"]);
			}
			if ("date_code" in module_info) {
				fields.set(_("Transceiver Date Code"), module_info["date_code"]);
			}
			if ("transceiver_types" in module_info) {
				let type_list = E('ul');
				let types = module_info["transceiver_types"];
				for(var i=0; i<types.length; i++) {
					type_list.appendChild(E('li', types[i]));
				}
				fields.set(_("Transceiver type(s)"), type_list);
			}
			if ("laser_wavelength" in module_info) {
				fields.set(_("Wavelength", module_info["laser_wavelength"]));
			}
			if ("tx_power" in module_info) {
				let tx_power_mW_string = module_info["tx_power"].toFixed(4) + " mW";
				let tx_power_dBm = 10 * Math.log10(module_info["tx_power"] * 1e-3) + 30;
				let tx_power_dBm_string = tx_power_dBm.toFixed(4) + " dBm";
				fields.set(_("TX Power"), tx_power_mW_string + " / " + tx_power_dBm_string);
			}
			if ("rx_power" in module_info) {
				let rx_power_mW_string = module_info["rx_power"].toFixed(4) + " mW";
				let rx_power_dBm = 10 * Math.log10(module_info["rx_power"] * 1e-3) + 30;
				let rx_power_dBm_string = rx_power_dBm.toFixed(4) + " dBm"
				fields.set(_("RX Power"), rx_power_mW_string + " / " + rx_power_dBm_string);
			}
			if ("bias_current" in module_info) {
				fields.set(_("Bias Current"), module_info["bias_current"].toFixed(3) + " mA");
			}
			if ("module_temp" in module_info) {
				fields.set(_("Module Temperature"), module_info["module_temp"].toFixed(2) + "°C");
			}
			if ("module_voltage" in module_info) {
				fields.set(_("Module Voltage"), module_info["module_voltage"] + " V")
			}
		}
		
		var table = E('table', {'id': 'sfp-overview', 'class': 'table' });

		for (const [key,value] of fields) {
			if (value == null || value == undefined)
			continue;
			table.appendChild(E('tr', { 'class': 'tr' }, [
				E('td', { 'class': 'td left', 'width': '33%' }, [ key ]),
				E('td', { 'class': 'td left' }, [ value ])
			]));
		}
		return table;
	},
	render: function(data) {
		var tabholder = E('div');
		var sfp_interfaces = Object.keys(data).sort();
		for (const interface_name of sfp_interfaces) {
			var thisInterfaceTable = this.renderOverviewTabTable(data[interface_name]); 
			var thisInterfaceTab = E('div', 
				{'data-tab': interface_name, 
				'data-tab-title': interface_name,
				}, [thisInterfaceTable]);
			tabholder.appendChild(thisInterfaceTab);
		}

		var header = E('h2', {}, _("SFP/Optical Transceivers"));
		var pane = E('div', {}, [header, tabholder]);

		ui.tabs.initTabGroup(tabholder.childNodes);
		return pane;
	}
});