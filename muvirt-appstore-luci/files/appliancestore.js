'use strict';
'require ui';
'require rpc';
'require uci';
'require form';
'require network';
'require view.muvirtwidgets';

var getApplianceList = rpc.declare({
  object: 'appliancestore',
  method: 'getlist',
  params: [],
});

var refreshList = rpc.declare({
  object: 'appliancestore',
  method: 'refresh',
  params: []
});

var deployApplianceCall = rpc.declare({
  object: 'appliancestore',
  method: 'deploy',
  params: ['newconfigid', 'source', 'applianceid']
});

return L.view.extend({
  processListResult: function(data) {
    this.applianceList = data;
    console.log("processListResult");
  },
  saveApplianceHandler: function(source, applianceData) {
    console.log("saveApplianceHandler: "+source);
    Promise.resolve(this.newApplianceMap.save()).then(
      function() {
          console.log("saved appliance map data");
          var data = this.newApplianceMap.data.data;
          var newApplianceImage = applianceData["id"];
          var newApplianceConfigID = data["vmquick"]["newvmid"];
          deployApplianceCall(newApplianceConfigID, source, newApplianceImage).then(this.redirectToVMEdit);
      }.bind(this)
    );
  },
  redirectToVMEdit: function(data) {
    console.log("redirectToVMEdit");
    var newSectionName = data["name"];
    var editPage = L.url("admin/muvirt/status")+"#"+newSectionName;
    window.location.href = editPage;
  },
  constructDeployApplianceModal: function(source, applianceData) {
    var formData = {
      vmquick: {
        newvmid: null,
        newvmcpu: 1,
        newvmmem: 512,
        newvmdisk: 4096,
      }
    };

    var m = new form.JSONMap(formData, _('New VM settings'), _('You can change more options in the next step'));
    s = m.section(form.NamedSection, 'vmquick','vmquick');
    var o = s.option(form.Value, 'newvmid', _('New VM ID'));
    o.default = applianceData["id"].replace(/-/g,"");

    /* o = s.option(form.Value, 'newvmcpu', _('Number of vCPUs'));
    o.datatype='uinteger';
    o = s.option(form.Value, 'newvmmem', _('RAM'), _('In megabytes'));
    o.datatype='uinteger';
    o = s.option(form.Value, 'newvmdisk', _('Disk Space'), _('New VM disk size'));
    o.datatype='uinteger'; */
    this.newApplianceMap = m;
    /* CBI Form elements, including JSONMap require a promise to render. So we tack the
     * rest of our modal construction at the end of the form render */
    return Promise.resolve(m.render()).then(function (contents) {
      var modalContent = E('div', {'class': 'appstore-deploy-modal-content'});
      var applianceInfo = E('div');
      applianceInfo.appendChild(E('p', [
        E('ul', [
          E('li', [
            E('b', _('Source repository: ')),
            E('span', source)
          ]),
          E('li', [
            E('b', _('Appliance: ')),
            E('span', applianceData["name"]),
            ' ',
            E('span', '('+applianceData["version"]+')')
          ]),
          E('li', [
            E('b', _('Maintainer: ')),
            E('span', applianceData["maintainer"])
          ])
        ])
      ]));
      modalContent.appendChild(applianceInfo);
      modalContent.appendChild(contents);

      var saveButton =  E('button', {'class': 'cbi-button cbi-button-positive important'}, _('Save appliance'));
      saveButton.onclick =  L.ui.createHandlerFn(this, 'saveApplianceHandler', source, applianceData);
      var discardButton = E('button', {'class': 'cbi-button'}, _('Dismiss'));
      discardButton.onclick = function() {
        L.ui.hideModal();
      };

      modalContent.appendChild(E('div', {'class': 'right'}, [
        saveButton,
        discardButton
      ]));
      return modalContent;
    }.bind(this))
  },
  deployApplianceButtonPressed: function(source, applianceData, evt) {
    var applianceId = applianceData["id"];
    console.log("deployApplianceButtonPressed, source="+source, " appliance="+applianceId);
    var deployModalContent = this.constructDeployApplianceModal(source, applianceData);
    Promise.resolve(deployModalContent).then(function (content) {
      L.ui.showModal(_("Deploy appliance"), content);
    });
  },
  renderApplianceList: function() {
    var applianceList = E('div', {'class': 'table cbi-section-table', 'id': 'appstore-appliance-list'});
      for(var source in this.applianceList) {
        var thissourceappliances = this.applianceList[source];
        thissourceappliances.forEach((item, i) => {
          var name = item["name"];
          var link = item["link"];
          var maintainer = item["maintainer"];
          var id = item["id"];
          var version = item["version"];
          var description = item["description"];
          var deployActionButton = E('button', {'class': 'cbi-button'}, _('Deploy'));
          deployActionButton.onclick = L.ui.createHandlerFn(this, 'deployApplianceButtonPressed', source, item);
          var appliance = E('div', {'class': 'tr cbi-section-table-row', 'style': 'border-bottom: 1px solid black;'}, [
              E('div', {'class': 'td cbi-value-field'}, [
                E('p', [
                  E('span', {'class': 'label appstore-source-label'}, source),
                  ' ',
                  E('b',name),
                  ' ',
                  E('span',version),
                ]),
                E('p', description),
                E('p', {'style': 'font-size: 0.8em'}, [
                  E('div',{'style': 'display: inline'}, [
                    E('span', _('Maintainer: ')),
                    E('span', maintainer)
                  ]),
                  ' ',
                  E('a',{'href': link}, _('Appliance homepage'))
                ]),
              ]),
              E('div', {'class': 'td cbi-section-table-cell nowrap cbi-section-actions'}, [
                deployActionButton
              ]),
            ]);
          applianceList.appendChild(appliance);
        });
      }
      return applianceList;
  },

  doRender: function() {
    m = new form.Map('appstore', _('Appliance Store'));
    m.tabbed = true;

    /* We can't add non-bound tabs to a form.Map so use a trick here,
     * use a section that is never defined and render in the placeholder
     */
    s = m.section(form.TypedSection, 'appliances', _('Appliances'));
    s.anonymous = true;
    s.addremove = false;
    s.renderSectionPlaceholder = function() {
      var applianceListDiv = this.renderApplianceList();
      return applianceListDiv;
    }.bind(this);

    s.renderSectionAdd = this.renderRefreshButton.bind(this);

    s = m.section(form.TypedSection, 'store', _('Repository'))
    s.addremove = true;
    s.anonymous = false;

    var o = s.option(form.Value, 'url', _('List URL'));
    o = s.option(form.Flag, 'enable', _('Enabled'), _("Note: When disabled, list will be removed on next refresh"));
    o = s.option(form.DummyValue, 'lastupdated', _("Last updated"));
    o.default = 'placeholder';

    s = m.section(form.TypedSection, 'appstore', _('Global Options'))
    s.addremove = false;
    s.anonymous = false;

    o = s.option(form.Value, 'listcache', _("List cache location"), _("Lists from repositories will be cached here"));

    return m.render();
  },
  renderRefreshButton: function() {
    var refreshButton = E('button',{'class':'cbi-button'},_("Refresh"));
    refreshButton.onclick = L.ui.createHandlerFn(this, 'callRefreshList');
    return refreshButton;
  },
  callRefreshList: function(event) {
    return refreshList().then(this.handleListRefresh.bind(this));
  },
  handleListRefresh: function(data) {
    var newApplianceList = getApplianceList().then(this.processListResult.bind(this)).then(this.renderApplianceList.bind(this)).then(function(newApplianceList) {
      var existingApplianceList = document.getElementById("appstore-appliance-list");
      var existingListParent = existingApplianceList.parentElement;
      existingListParent.replaceChild(newApplianceList, existingApplianceList);
    });
  },
  render: function() {
    var applianceListPopulate = getApplianceList().then(this.processListResult.bind(this));
    return Promise.resolve(applianceListPopulate).then(this.doRender.bind(this));
  }
});
