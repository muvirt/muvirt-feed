'use strict';
'require ui';
'require rpc';
'require uci';
'require form';
'require network';
'require poll';
'require view.muvirtwidgets';
'require tools.widgets as widgets';

var callSpawnConsole = rpc.declare({
  object: 'muvirt',
  method: 'console',
  params: ['vmname'],
});

var callStartVM = rpc.declare({
  object: 'muvirt',
  method: 'start',
  params: ['vmname'],
});

var callStopVM = rpc.declare({
  object: 'muvirt',
  method: 'stop',
  params: ['vmname'],
});

var getStorageStatus = rpc.declare({
  object: 'muvirt',
  method: 'storageinfo',
  params: []
});

var getVMStatus = rpc.declare({
  object: "muvirt",
  method: "status",
  params: []
});

const getSetupBlockDeviceCall = rpc.declare({
  object: "muvirt",
  method: "get_setup_block_devices",
  params: []
});

const doSetupCall = rpc.declare({
  object: "muvirt",
  method: "do_setup",
  params: ["device","mode","swapsize","workspace"]
})

const isSetupCall = rpc.declare({
  "object": "muvirt",
  "method": "is_setup",
  params: []
});

var setupFormData = {
  setup: {
    diskdevice: null
  }
};

var vmoverview = null;
function generateRandomBaseMAC() {
  var mac = "52:54:00"; // QEMU OUI
  for (var i = 0; i < 3; i++) {
    var nibble = Math.round(Math.random() * 255).toString(16);
    if (nibble.length == 1) {
      nibble = "0" + nibble;
    }
    mac = mac + ":" + nibble;
  }
  return mac;
}
function handleSpawnConsoleResponse(resp) {
  open(location.protocol + "//" + resp.username + ':' + resp.password + "@" + location.host + "/vmconsole/" + resp.vmname + "/" + resp.sessionid + "/");
}

return L.view.extend({
  renderOverviewRowActions: function (section_id) {
    var tdEl = this.super('renderRowActions', [section_id, _('Edit')]);

    var stopStartButton = E('button',
      { 'type': 'button', 'class': 'cbi-button', 'data-button-type': 'stopStart', 'data-vmname': section_id },
      _('Start'));
    stopStartButton.disabled = true; /* Will be enabled by poll refresh */

    stopStartButton.onclick = L.ui.createHandlerFn(this.parentCaller, 'handleVMActionButton');

    var consoleButton = E('button', { 'type': 'button', 'data-button-type': 'console', 'class': 'cbi-button' }, _('Console'));
    consoleButton.onclick = L.ui.createHandlerFn(this.parentCaller, 'spawnVMConsole', section_id);
    consoleButton.disabled = true;

    L.dom.content(tdEl.lastChild, [
      stopStartButton,
      consoleButton,
      tdEl.lastChild.firstChild,
      tdEl.lastChild.lastChild
    ]);
    return tdEl;
  },
  render: function () {
    var m, s, o;
    m = new form.Map('virt', _('Virtualization'));
    m.tabbed = true;

    var vmstatus = this.vmstatus;

    s = m.section(form.GridSection, 'vm', _('Virtual Machines'));
    s.addremove = true;
    s.anonymous = false;
    s.addbtntitle = _('Add new virtual machine');
    s.vmstatus = this.vmstatus;
    s.parentCaller = this;
    s.renderRowActions = this.renderOverviewRowActions;

    this.gridsection = s;

    s.tab('general', _('General'))
    s.tab('network', _('Network'))
    s.tab('storage', _('Storage'))
    s.tab('passthrough', _('Passthrough'))
    s.tab('provisioning', _('Provisioning'))
    s.tab('cloudinit', _('cloud-init'))
    s.tab('misc', _('Miscellaneous'))

    s.filter = function (section_id) {
      return true;
    };



    s.modaltitle = function (section_id) {
      return _('Virtual Machines') + ' » ' + section_id.toUpperCase();
    }

    o = s.option(form.Flag, "_status", _("Status"));
    o.textvalue = function (s) {
      return _("(Refreshing)");
    };

    o.modalonly = false;

    o = s.option(form.DummyValue, '_resource', _('Resources'));
    o.modalonly = false;
    o.textvalue = this.renderResourceStatusCell;

    o = s.option(form.DummyValue, '_netstat', _('Network'));
    o.modalonly = false;

    /* Modal dialog only options */
    o = s.taboption('general', form.Value, 'numprocs', _('Number of vCPUs'));
    o.default = '1';
    o.modalonly = true;
    o.datatype = 'uinteger';

    o = s.taboption('general', form.Value, 'memory', _('RAM'));
    o.datatype = 'uinteger';
    o.default = '512';
    o.modalonly = true;

    o = s.taboption('general', form.Flag, 'ballooning', _('Enable memory ballooning'),
      _('Allows the host to request memory back from a virtual machine'));
    o.modalonly = true;

    o = s.taboption('general', form.Flag, 'enable', _('Enable this VM'),
      _('If unchecked, VM will not be started on boot'));
    o.modalonly = true;

    o = s.taboption('general', form.Flag, 'provisioned', _('Is this VM provisioned?'),
      _('If unchecked, the specified image (see Provisioning tab) will be downloaded on first use'));
    o.modalonly = true;


    o = s.taboption('network', form.Value, 'mac', _('Base MAC address'),
      _('Applied to the first interface and incremented thereafter. MACs can be overriden individually below'));
    o.modalonly = true;
    o.default = generateRandomBaseMAC();

    o = s.taboption('network', view_muvirtwidgets.OrderedNetworkSelect, 'network', _('Networks to connect to'))
    o.multiple = true;
    o.bridgeonly = true;
    //o.noaliases = false;
    o.filter = function (section_id, value) {
      console.log("Network select: " + section_id + " value: " + value);
      return true;
    };
    o.modalonly = true;

    o = s.taboption('storage', view_muvirtwidgets.DiskSelect, 'disks', _('Storage devices'));
    o.modalonly = true;

    o = s.taboption('passthrough', view_muvirtwidgets.USBPassthroughSelect, 'usbdevice', _('USB Devices to passthrough'));
    o.modalonly = true;

    o = s.taboption('passthrough', form.DynamicList, 'pcidevice', _('PCIe Devices to passthrough'), _('PCI bus IDs from lspci, like 0001:03:00.0'));
    o.modalonly = true;
    o.optional = true;

    o = s.taboption('passthrough', form.DynamicList, 'dpaa2container', _('DPAA2 DPRC container'), _('e.g dprc.2. Device tree boot (direct kernel or U-Boot) is required to use this feature'));
    o.modalonly = true;
    o.optional = true;

    o = s.taboption('provisioning', form.Value, 'imageurl', _('Image URL'),
      _('Image to download'));
    o.modalonly = true;

    o = s.taboption('provisioning', form.Value, 'checksum', _('Image Hash/Checksum'), _('in the format of sha256:abcdef or sha512:abcdef'));
    o.modalonly = true;

    o = s.taboption('provisioning', form.Value, 'imageformat', _('Image format'),_('Default qcow2, or raw-xz'));
    o.modalonly = true;

    o = s.taboption('provisioning', form.Value, 'disksize', _('Disk size'),
      _('In megabytes (MB). Image will be resized to this during provisioning'));
    o.datatype = 'uinteger';
    o.modalonly = true;

    o = s.taboption('provisioning', form.Flag, 'provinplace', _('Provision in place'),
      _('Write the image file to the configured storage device instead of creating one'));
    o.modalonly = true

    o = s.taboption('provisioning', form.Value, 'provisioner', _('Provisioning handler to use'),
    _('Specify a provisioning handler to use instead of the default (cloudinit), for example &quot;microos&quot;'));
    o.modalonly = true

    o = s.taboption('provisioning', form.Value, 'combustion_template', _('microOS Combustion Template'),
    _('Template for use with the microOS provisioner. Examples are &quot;greengrass&quot; and &quot;k3s&quot;'));
    o.modalonly = true
  
    o = s.taboption('cloudinit', form.Flag, 'needscloudinit', _('Enable cloud-init creation'),
    _('Enable if the guest VM needs a cloud-init file to set up'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.Value, 'userpw', _('User password'),
      _('(Only for cloud-init). Will be deleted from UCI after provisioning'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.Flag, 'userpwexpire', _('Don\'t force password change?'),
      _('By default, the cloud-init user will have to change their password on first login'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.Value, 'cchostname', _('Hostname'),
      _('(Only for cloud-init). By default, hostname will be the same as VM name in UCI'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.DynamicList, 'sshkey', _('SSH keys'),
      _('One per line, OpenSSH format'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.FileUpload, 'script', _('Script to run on first boot (not yet implemented)'),
      _('Installing packages etc.'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.Value, 'cclocale', _('Locale'),
      _('Change the operating system locale, e.g en_AU, de_DE'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.FileUpload, 'cloudinit', _('Cloud-init file'),
      _('NOTE: Not used if any of the above options are set'));
    o.modalonly = true;

    o = s.taboption('cloudinit', form.DynamicList, 'config_data', _('Configuration Data'),
      _('Key=value format pairs to be passed to the cidata image creator. Will be deleted from uci after provisioning'));
    o.modalonly = true;

    o = s.taboption('misc', form.Value, 'shutdowntimeout', _('Graceful shutdown timeout'),
      _('Default 60 seconds'));
    o.modalonly = true;

    o = s.taboption('misc', form.Value, 'bios', _('EFI binary override'),
      _('Default is /lib/muvirt/QEMU_EFI.fd'));
    o.modalonly = true;

    /* End tab - start storage options */
    s = m.section(view_muvirtwidgets.StorageSection, 'storage', _('Storage'));
    s.addremove = false;
    s.anonymous = false;

    //o = s.option(form.Value, 'driver', _('Storage driver name'));
    //o = s.option(form.Value, 'device', _('Storage device'));

    /* End tab - start global options */
    s = m.section(form.TypedSection, 'muvirt', _('Global options'))
    s.addremove = false;
    s.anonymous = false;

    o = s.option(form.Value, 'hugetlb', _('HugeTLB area size'),
      _("Reserve an area of memory for better virtual machine performance. " +
      "Values less than 1.0 will be interpreted as a percentage of system memory."));
    o.datatype = 'ufloat';

    o = s.option(form.Value, 'scratch', _('Scratch area path'), _("Scratch folder to download virtual machine images for provisioning"));

    o = s.option(widgets.NetworkSelect, 'defaultnet', _('Default VM network'), _('Default network for new virtual machines'));
    o.default = 'lan';
    o.rmempty = true;
    o.multiple = false;
    o.novirtual = true;

    var pollStatusFunction = L.bind(this.poll_status, this);
    poll.add(pollStatusFunction);

    /* Call is_setup first and return the normal render chain if the storage
     * driver has been setup.
     * If not, append the 'do setup' banner after
     */
    const normalRenderFunction = m.render().then(L.bind(this.goToHashEdit,this));
    const renderNotificationFunction = L.bind(this.renderDoSetupNotification,this);
    return isSetupCall().then(function (return_data) {
        console.log("is_setup: " + JSON.stringify(return_data));
        if (return_data["is_setup"] == true) {
          return normalRenderFunction;
        } else {
          return normalRenderFunction.then(renderNotificationFunction);
        }
    });
  },
  renderDoSetupNotification: function (maincontent) {
    const setupButton = E("button",_("Do system setup..."));
    setupButton.onclick = L.bind(this.showSetupModal, this);

    const setupButtonDiv = E("div", [setupButton]);
    const alertDiv = E("div", [
      E("p",
        [
          E("span",
            _("Partition a disk drive to use edge computing and automated provisioning features"))
        ]),
      setupButtonDiv
    ]);

    this.setupNotification = L.ui.addNotification(_("Virtualization Storage Setup"), alertDiv);
    return maincontent;
  },
  poll_status: function () {
    getVMStatus().then(this.processStatus.bind(this));
  },
  createVMStatusText: function (status) {
    var cls = "label";
    var style = "";
    switch (status) {
      case "on":
        style = "background-color: green";
        break;
      case "download-failed":
      case "start-failed":
        cls += " warning";
        break;
      case "off":
        style = "background-color: #777;";
    }
    return E('span', { "class": cls, "style": style }, _(status));
  },
  updateVMActionButton: function (actionButtonCell, vmstatus) {
    var stopStartButton = actionButtonCell.querySelector("button[data-button-type=\"stopStart\"]");
    var consoleButton = actionButtonCell.querySelector("button[data-button-type=\"console\"]");
    var vmrunning = ("running" in vmstatus && vmstatus["running"] == true) ? true : false;
    consoleButton.disabled = !vmrunning;
    stopStartButton.disabled = false; /* These are disabled by default until the XHR poll cycle (i.e now) */
    if (vmrunning) {
      stopStartButton.innerText = _('Stop');
      stopStartButton.class = 'cbi-button-negative';
      stopStartButton.setAttribute("data-action", "stop");
    } else {
      stopStartButton.setAttribute("data-action", "start");
      stopStartButton.innerText = _('Start');
    }
  },
  processStatus: function (status) {
    for (var vmname in status) {
      var vmstatus = status[vmname];
      var vmrowId = "cbi-virt-" + vmname;
      var vmrow = document.getElementById(vmrowId);
      if (vmrow == null) {
        continue;
      }
      var vmStatusCell = vmrow.querySelector("[data-name=\"_status\"]");
      vmStatusCell.innerHTML = "";
      vmStatusCell.appendChild(this.createVMStatusText(vmstatus["status"]));

      var vmNetworkCell = vmrow.querySelector("[data-name=\"_netstat\"]");
      if ("network" in vmstatus) {
        vmNetworkCell.innerHTML = ("" + vmstatus["network"]).replace("\n", "<br/>");
      } else {
        vmNetworkCell.innerHTML = "";
      }

      var actionsCell = vmrow.querySelector(".cbi-section-actions div");
      this.updateVMActionButton(actionsCell, vmstatus);
    }
  },
  goToHashEdit: function (render) {
    var hash = window.location.hash;
    if (hash != undefined && hash.length > 0) {
      var editSection = hash.substring(1);
      this.gridsection.renderMoreOptionsModal(editSection);
    }
    return render;
  },
  processStatusResponse: function (response) {
    this.vmstatus = response;
  },
  renderResourceStatusCell: function (vmname) {
    /* TODO: This is bad */
    var vmconfig = this.section.map.data.state.values["virt"][vmname];

    var resourcetext = vmconfig["numprocs"] + " vCPU<br/>" +
      vmconfig["memory"] + "M";

    var node = E('div', {
      'id': '%s-ifc-description'.format(vmname)
    }, resourcetext);
    return node;
  },
  spawnVMConsole: function (vmname, event) {
    return callSpawnConsole(vmname).then(handleSpawnConsoleResponse);
  },
  checkChangesBeforeStarting: function() {
    var uiChanges = ui.changes;
    var numChanges = Object.keys(uiChanges.changes).length;
    if (numChanges > 0) {
      uiChanges.displayChanges();
      return true;
    }
    return false;
  },
  handleVMActionButton: function (event) {
    var target = event.target;
    target.disabled = true;
    var vmname = target.getAttribute("data-vmname");
    var action = target.getAttribute("data-action");
    if ("stop" == action) {
      target.innerText = _("Stopping");
      callStopVM(vmname);
    } else if ("start" == action) {
      // Check if any config changes are pending,
      // we don't want to start a VM if there are
      // (e.g setting details after appstore provision)
      var hasChanges = this.checkChangesBeforeStarting();
      if (!hasChanges) {
        target.innerText = _("Starting");
        callStartVM(vmname);
      }
    }
  },
  handleSetupCallReturn(rpc_return) {
    console.log("handleSetupCallReturn: " + JSON.stringify(rpc_return));
    let setupSuccess = true;
    if (typeof(rpc_return) == "object" && "error" in rpc_return) {
      const errorMessage = rpc_return["error"];
      alert(_("ERROR: " + errorMessage));
      setupSuccess = false;
    }
    if (typeof(rpc_return) != "object")
      setupSuccess = false;

    L.ui.hideModal();
    if (setupSuccess)
      L.ui.addNotification("Setup complete", E("span",_("Storage setup complete")), "success");
    else
      L.ui.addNotification("Setup failed", E("span", _("Storage setup failed. Please check the system log, or run \"muvirt-system-setup\" manually")), "danger");

    if (this.setupNotification) {
      this.setupNotification.parentNode.removeChild(this.setupNotification);
    }
  },
  doSetup: function(evt, target) {
    console.log("doSetup()");
    const parameters = setupFormData["setup"];
    const diskDeviceName = parameters["diskdevice"];
    const workSize = parameters["worksize"];
    const swapSize = parameters["swapsize"];
    const matchingDiskDevice = this.diskDeviceData[diskDeviceName];
    const diskPartitionMode = (matchingDiskDevice["rootdevice"]) ? "samedisk" : "fulldisk";
    const setupCallReturnBound = L.bind(this.handleSetupCallReturn,this);
    doSetupCall(diskDeviceName,diskPartitionMode,swapSize, workSize).then(setupCallReturnBound);
  },
  renderSetupForm: function(disk_devices) {
    this.setupMap = new form.JSONMap(setupFormData, _("System Setup"));
    s = this.setupMap.section(form.NamedSection, 'setup','setup');
    /* s.addremove = false;
    s.addbtntitle = _("Do setup"); */

    let o = s.option(form.ListValue, "diskdevice", _("Disk Device"));
    this.diskDeviceData = disk_devices;
    Object.keys(disk_devices).forEach(function (drivename) {
      const driveData = disk_devices[drivename];
      let formattedString = drivename + " (" +  driveData["size"] + "GiB ";
      formattedString = formattedString + driveData["model"];
      formattedString = formattedString + _("S/N") + " " + driveData["serial"];
      formattedString = formattedString +" )";
      o.value(drivename,formattedString);
    });

    o = s.option(form.Value, "worksize", _("Work Partition Size"));
    o.datatype = "uinteger";
    o.default = 10240;

    o = s.option(form.Value, "Swap partition size", _("Swap partition size"));
    o.datatype = "uinteger";
    o.default = 10240;

    return this.setupMap.render().then();
  },
  renderSetupModal: function(formContent) {
    const submitButton = E("button",_("Proceed with setup"));
    submitButton.onclick = function() {
      this.setupMap.save().then(this.doSetup.bind(this));
    }.bind(this);
    const submitPlaceholder = E("div", [submitButton]);
    const modalDiv = E("div", [formContent, submitPlaceholder]);
    return modalDiv;
  },
  showSetupModal: function() {
    const renderSetupFunction = L.bind(this.renderSetupForm,this);
    const setupModalRenderFunction = L.bind(this.renderSetupModal,this);
    return getSetupBlockDeviceCall().
      then(renderSetupFunction).
      then(setupModalRenderFunction).
      then(
        function(content) {
          L.ui.showModal(_("muvirt storage setup"), content);
        }
      );
  },
  handleSaveApply: null,
  handleReset: null
});
