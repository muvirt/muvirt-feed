#!/bin/sh
. "/lib/muvirt/provfuncs.sh"
set +x

vmname="${1}"

# Noop - k3os is done via a cd install
write_image() {
    vmname="${1}"
    vm_imagefile="${2}"

    isomount="${VM_SCRATCH_DIR}/iso"
    kernelmount="${VM_SCRATCH_DIR}/kernel"

    mkdir -p "${isomount}"
    mkdir -p "${kernelmount}"

    mount -t iso9660 -o loop "${vm_imagefile}" "${isomount}" || (deferred_error "${vmname}" "Failed to mount k3os iso" && return 1)
    mount -t squashfs -o loop "${isomount}/k3os/system/kernel/current/kernel.squashfs" "${kernelmount}" || (deferred_error "${vmname}" "Failed to read k3os kernel image" && return 1)

    cp "${kernelmount}/vmlinuz" "${VM_SCRATCH_DIR}/vmlinuz"
    umount "${kernelmount}"

    cp "${isomount}/k3os/system/kernel/current/initrd" "${VM_SCRATCH_DIR}/initrd"
    umount "${isomount}"

    uci set "virt.${vmname}.cdrom=${vm_imagefile}"
    return 0
}

create_cloud_config() {
    vmname="${1}"
    vm_scratchdir="${2}"

    mkdir -p "${vm_scratchdir}/config/"
    /usr/libexec/muvirt/muvirt-create-k3os-config "${vmname}" "${vm_scratchdir}/config/k3os.yml"

    uci set "virt.${vmname}.tftp=${vm_scratchdir}/config"
}

do_vm_firstboot() {
    vmname="${1}"

    k3os_yml_url="tftp://10.0.2.2/k3os.yml"

    uci set "virt.${vmname}.kernel=${VM_SCRATCH_DIR}/vmlinuz"
    uci set "virt.${vmname}.initrd=${VM_SCRATCH_DIR}/initrd"
    uci set "virt.${vmname}.append=console=ttyAMA0 k3os.install.tty=ttyAMA0 k3os.mode=install k3os.install.config_url=${k3os_yml_url} k3os.install.device=/dev/vda k3os.install.power_off=true k3os.install.force_efi=true k3os.install.silent=true"
    uci set "virt.${vmname}.oneshot=1"

    uci show "virt.${vmname}"

    start_and_wait_for_vm_to_exit "${vmname}"

    logger -t "muvirt-provision" -s "[${vmname}] First boot VM done"

    uci delete "virt.${vmname}.kernel" || :
    uci delete "virt.${vmname}.initrd" || :
    uci delete "virt.${vmname}.append" || :
    uci delete "virt.${vmname}.cdrom" || :
    uci delete "virt.${vmname}.oneshot" || :
    uci delete "virt.${vmname}.tftp" || :
}