local uci = require "luci.model.uci".cursor()
local util = require "luci.util"
local plpath = require "pl.path"
local plfile = require "pl.file"
local pldir = require "pl.dir"
local plutil = require "pl.utils"
local stringx = require "pl.stringx"
local muvirt = {}

function muvirt.get_instances()
	local knownvms = {}
	local instances = util.ubus("service","list", {name = "muvirt"})
	local configs = uci:foreach("virt","vm", function(s)
		local name = s[".name"]
		properties = {}
		properties["enabled"] = (s.enable == "1" and true or false)
		properties["provisioned"] = (s.provisioned == "1" and true or false)
		properties["numprocs"] = (s.numprocs and tonumber(s.numprocs) or 1)
		properties["mem"] = (s.memory and tonumber(s.memory) or 0)
		if (s["_downloading"] ~= nil and s["_downloading"] == "1") then
			properties["status"] = "downloading"
		elseif (s["_downloading"] ~= nil and s["_downloading"] == "-1") then
			properties["status"] = "download-failed"
		elseif (s["_configuring"] ~= nil and s["_configuring"] == "1") then
			properties["status"] = "configuring"
		elseif (s["_configuring"] ~= nil and s["_configuring"] == "-1") then
			properties["status"] = "configure-failed"
		elseif (s.provisioned == "0" and true or false) then
			properties["status"] = "to-be-provisioned"
		elseif (s.enable == "0" and true or false) then
			properties["status"] = "disabled"
		else
			properties["status"] = "off"
		end
		properties["running"] = false
		knownvms[name] = properties
	end
	)
	if instances.muvirt ~= nil and instances.muvirt.instances ~= nil then
		allinstances = instances["muvirt"]["instances"]
		for vmname, vmprops in pairs(allinstances) do
			running = vmprops["running"]
			-- Make sure the VM is in the current UCI state - if deleted but
			-- previously running it might not be.
			if knownvms[vmname] ~= nil then
				knownvms[vmname]["running"] = running
				if running == true then
					if (knownvms[vmname]["status"] ~= "configuring") then
						knownvms[vmname]["status"] = "on"
					end
					knownvms[vmname]["pid"] = vmprops["pid"]
					local net_info_pipe = io.popen("/lib/muvirt/muvirt-guest-info-web %s" % luci.util.shellquote(vmname))
					local net_info = net_info_pipe:read("*a")
					knownvms[vmname]["network"] = net_info
					net_info_pipe:close()
				elseif (vmprops["exit_code"] ~= nil and vmprops["exit_code"] > 0) then
					exitcode = vmprops["exit_code"]
					knownvms[vmname]["status"] = "start-failed"
				end
			end
		end
	end
  return knownvms
end

function muvirt.generate_random_mac()
  math.randomseed(os.time())
  local firstOctet = math.random(0,255)
  local secondOctet = math.random(0,255)
  local thirdOctet = math.random(0,255)
  local formattedMac = string.format("52:54:00:%02X:%02X:%02X", firstOctet, secondOctet, thirdOctet)
  return formattedMac
end

function muvirt.generate_disk_serial()
	math.randomseed(os.time())
	local firstOctet = math.random(0,255)
	local secondOctet = math.random(0,255)
	local thirdOctet = math.random(0,255)
	local fourthOctet = math.random(0,255)
	local formattedSN = string.format("%02X%02X%02X%02X", firstOctet, secondOctet, thirdOctet, fourthOctet)
	return formattedSN
end

function muvirt.get_default_network()
  defaultnet = nil
  uci:foreach("virt","muvirt", function(s)
      name = s[".name"]
      if (s["defaultnet"] ~= nil and defaultnet == nil) then
        defaultnet = s["defaultnet"]
      end
    end)
  if (defaultnet == nil) then
    defaultnet = "br-lan"
  end
  return defaultnet
end

function muvirt.is_device_bridge(devicename)
	local is_bridge=false
	configs = uci:foreach("network","device",
		function(s)
			name = s["name"]
			type = s["type"]
			if (devicename == name) then
				if (type == "bridge") then
					is_bridge=true
				end
			end
		end
	)
	return is_bridge
end

function muvirt.get_workdir()
	workdir = nil
	uci:foreach("virt","muvirt", function(s)
		name = s[".name"]
		if (s["scratch"] ~= nil and workdir == nil) then
			workdir = s["scratch"]
		end
	  end)
	if (workdir == nil) then
		workdir = "lan"
	end
	return workdir
end

function muvirt.get_library_dir()
	local workdir = muvirt.get_workdir()
	return workdir .. "/imglibrary"
end

function muvirt._log_to_syslog(message)
	local quotedmsg = util.shellquote(message)
	util.exec("logger -t virt -p daemon.info " .. quotedmsg)
end

function muvirt._get_configured_hugepages()
	hugepageval = nil
	uci:foreach("virt","muvirt", function(s)
		name = s[".name"]
		if (s["hugetlb"] ~= nil and hugepageval == nil) then
			hugepageval = s["hugetlb"]
		end
	  end)
	return hugepageval
end

function muvirt._setup_hugetlb(hugetlb_size_mb)
	local hugetlb_size_in_pages = math.floor((hugetlb_size_mb * 1024) / (2048))
	muvirt._log_to_syslog("Setting HugeTLB pages to " .. hugetlb_size_in_pages .. " (" .. math.floor(hugetlb_size_mb) .. " MB)")

	plfile.write("/proc/sys/vm/nr_hugepages", ""..hugetlb_size_in_pages)
	pldir.makepath("/tmp/hugetlbfs")
	util.exec("mount -t hugetlbfs none /tmp/hugetlbfs")
end

function muvirt.do_hugetlb_setup()
	local sysinfo = util.ubus("system", "info", {})
	local meminfo = sysinfo["memory"]
	local total_memory = meminfo["total"] / (1024.0 * 1024.0)
	local has_hugepages = plpath.exists("/proc/sys/vm/nr_hugepages")
	local configured_hugepageval = muvirt._get_configured_hugepages()
	local default_pct = 0.85 * total_memory
	if (configured_hugepageval ~= nil) then
		local c_hugepageval_num = tonumber(configured_hugepageval)
		-- Test if configured hugepageval is greater than current memory
		if (c_hugepageval_num < total_memory) then
			if (c_hugepageval_num > 1) then
				muvirt._setup_hugetlb(c_hugepageval_num)
				return
			else
				local hugepagepct = c_hugepageval_num * total_memory
				muvirt._setup_hugetlb(hugepagepct)
				return
			end
		end -- Fall through to default
		muvirt._log_to_syslog("Configured HugeTLB memory ("..c_hugepageval_num..") is higher than available, ignoring")
	end
	muvirt._setup_hugetlb(default_pct)
	return
end

function muvirt.get_work_free_space() 
	local workdir = muvirt.get_workdir()
	if (workdir == nil) then
		return 0
	end
	local df_success, df_return, df_stdout = plutil.executeex("df -Pm " .. plutil.quote_arg(workdir))
	if (df_success ~= true) then
		return 0
	end
	local df_lines = stringx.split(df_stdout, '\n')
	if (#df_lines < 2) then
		return 0
	end
	local our_line = df_lines[2]

	local device,capacity,used,mount = our_line:match("([\\/%w_-]+)[ \t]+(%d+)[ \t]+(%d+)[ \t]+%d+[ \t]+[%d%%]+ ([\\/%w_-]+)")
	local free = capacity-used
	return free,capacity,device,mount
end

function muvirt.get_work_free_space_dict()
	local free,capacity,device,mount = muvirt.get_work_free_space()
	if (free == 0) or (device == nil) then
		return {}
	end
	local device_array = {}
	device_array["free"] = free
	if (capacity ~= nil) then
		device_array["capacity"] = capacity
	end
	if (mount ~= nil) then
		device_array["mount"] = mount
	end
	local return_array = {}
	return_array[device] = device_array
	return return_array
end

return muvirt
