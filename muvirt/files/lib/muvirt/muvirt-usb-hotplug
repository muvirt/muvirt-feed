#!/usr/bin/lua
local lfs = require "lfs"
local uci = require "luci.model.uci".cursor()
local stdlib = require 'posix.stdlib'

-- MD5 is only used to do (filesystem-safe) key hashing - not for cryptographic use
local md5 = require "md5"

function readDescriptorFile(path)
  local f = io.open(path)
  local s = f:read("*a")
  f:close()
  stripped = s.gsub(s, "\n", "")
  return stripped
end

function writeVMAssociation(vmname, hotplugpath, deviceid)
  local writepath = "/tmp/run/muvirt-usb/"..md5.sumhexa(hotplugpath)
  local f = io.open(writepath, "w")
  local writecontents = vmname.." "..deviceid
  f:write(writecontents)
  f:close()
end

function findVMForDevice(hotplugpath)
  local hashed = md5.sumhexa(hotplugpath)
  local linkfilepath = "/tmp/run/muvirt-usb/"..hashed
  local pathfile = io.open(linkfilepath,"r")
  if pathfile == nil then
    return nil
  end
  local vmdata = pathfile:read("*a")
  pathfile:close()
  local strippedstr = vmdata:gsub("\n","")
  local delimloc = strippedstr:find(" ");
  local vmname = strippedstr:sub(1,delimloc-1)
  local deviceid = strippedstr:sub(delimloc+1)
  return vmname, deviceid, linkfilepath
end

function findDeviceBySerial(serial)
  for file in lfs.dir("/sys/bus/usb/drivers/usb/") do
    if (file:sub(1, 3) ~= "usb") then
      local devpath = "/sys/bus/usb/drivers/usb/" .. file
      local attr = lfs.symlinkattributes  (devpath)

      if attr ~= nil and attr.target ~= nil then
        serialpath = devpath .. "/serial"
        if (lfs.attributes(serialpath, "mode") ~= nil) then
          local thisDeviceSerial = readDescriptorFile(serialpath)
          if thisDeviceSerial == serial then
            return devpath
          end
        end
      end
    end
  end
end

function findDeviceByVendorProductID(vendor,product)
  vendorHex = string.format("%04x", vendor)
  productHex = string.format("%04x", product)
  for file in lfs.dir("/sys/bus/usb/drivers/usb/") do
    if (file:sub(1, 3) ~= "usb") then
      local devpath = "/sys/bus/usb/drivers/usb/" .. file
      local attr = lfs.symlinkattributes  (devpath)

      if attr ~= nil and attr.target ~= nil then
        vendorpath = devpath .. "/idVendor"
        productpath = devpath .. "/idProduct"
        if (lfs.attributes(vendorpath, "mode") ~= nil) and (lfs.attributes(productpath, "mode") ~= nil) then
          local thisVendorHex = readDescriptorFile(vendorpath)
          local thisProductHex = readDescriptorFile(productpath)
          if (thisVendorHex == vendorHex) and (thisProductHex == productHex) then
            return devpath
          end
        end
      end
    end
  end
end


function getQEMUArguments(devpath)
  busnumpath = devpath .. "/busnum"
  if (lfs.attributes(busnumpath, "mode") == "file") then
    busnum = readDescriptorFile(devpath .. "/busnum")
    devnum = readDescriptorFile(devpath .. "/devnum")
    local deviceid = "usb"..busnum..devnum
    local qemuaddstr = "usb-host,id="..deviceid..",hostbus="..busnum..",hostaddr="..devnum
    return qemuaddstr,deviceid
  end
end

function findMatchingDeviceFromSpec(usbspec)
  local argstable = {}
  local startloc = 0
  local nextdelim = 0
  while true do
    nextdelim = usbspec:find(",", startloc)
    local fragment = nil
    if (nextdelim == nil) then
      fragment = usbspec:sub(startloc)
    else
      fragment = usbspec:sub(startloc, nextdelim-1)
      startloc = nextdelim+1
    end
    for fragkey,fragval in fragment:gmatch("(%w+)=(.+)") do
      argstable[fragkey] = fragval
    end
    if (nextdelim == nil) then
      break
    end
  end


  if argstable["serial"] ~= nil then
    serial=argstable["serial"]
    matchingDevice = findDeviceBySerial(serial)
    return matchingDevice
  elseif argstable["vendor"] ~= nil and argstable["product"] ~= nil then
    vendorID = argstable["vendor"]
    productID = argstable["product"]
    matchingDevice = findDeviceByVendorProductID(vendorID, productID)
    return matchingDevice
  elseif argstable["device"] ~= nil then
    local devpath = "/sys/bus/usb/drivers/usb/"..argstable["device"]
    return devpath
  end
end

function doUSBSpec()
  matchingDevicePath = findMatchingDeviceFromSpec(arg[2])
  local vmname = nil
  if (#arg >= 3) then
    vmname = arg[3]
  end
  if (matchingDevicePath ~= nil) then
    local qemuArgs, deviceID = getQEMUArguments(matchingDevicePath)
    print(qemuArgs)

    -- Register the device to the VM
    if (vmname ~= nil) then
      local realdevpath = stdlib.realpath(matchingDevicePath)
      realdevpath = realdevpath:sub(5)
      print(realdevpath)
      writeVMAssociation(vmname, realdevpath, deviceID)
    end
  else
    os.exit(1)
  end
end

function findMatchingVM(devpath)
  local matchingVM = nil
  local matchingDevice = nil
  uci:foreach("virt","vm", function(s)
    name = s[".name"]
    if (s["usbdevice"] ~= nil) then
      local usbdevice = s["usbdevice"]
      for idx, configvalue in ipairs(usbdevice) do
        deviceMatch = findMatchingDeviceFromSpec(configvalue)
        if deviceMatch ~= nil then
          local realDeviceMatch = stdlib.realpath(deviceMatch)
          if (devpath == realDeviceMatch) then
            matchingVM = name
            matchingDevice = deviceMatch
          break
          end
        end
      end
    end
  end
  )
  return matchingVM, matchingDevice
end


function doHotplugAdd(hotplugpath)
  local fulldevpath="/sys"..hotplugpath
  if (lfs.attributes(fulldevpath, "mode") ~= "directory") then
    print("Invalid device path: "..fulldevpath)
    os.exit(1)
  end

  -- Iterate over all VM USB devices until a match is found
  vmmatch, devpath = findMatchingVM(fulldevpath)
  print("Matching VM: "..vmmatch)
  if (vmmatch ~= nil) then
    local qemuString, deviceId = getQEMUArguments(devpath)
    local result = os.execute("/usr/sbin/muvirt-command ".. vmmatch .. " device_add " .. qemuString)
    writeVMAssociation(vmmatch, hotplugpath, deviceId)
  end
end

function doHotplugRemove(hotplugpath)
  local vm, deviceid, linkfile = findVMForDevice(hotplugpath)
  if vm ~= nil then
    local result = os.execute("/usr/sbin/muvirt-command " .. vm .. " device_del " .. deviceid)
    os.remove(linkfile)
  end
end

function doHotplug()
  local hotplugaction=arg[2]
  local hotplugpath=arg[3]
  print("Hotplug: "..hotplugaction.. " path "..hotplugpath)
  if (hotplugaction == "add") then
    doHotplugAdd(hotplugpath)
  elseif (hotplugaction == "remove") then
    doHotplugRemove(hotplugpath)
  end
end

function printUsage()
  print("Usage: muvirt-usb-utility <operation> arguments..")
  print("\t usbspec <uci-config-string> (vmname-for-linking)")
  print("\t hotplug <action> <devpath>")
end

function isUbusRunning()
  local ubusmode = lfs.attributes("/tmp/run/ubus/ubus.sock","mode")
  if (ubusmode == 'socket') then
    return true
  end
  return false
end

if #arg < 1 then
  printUsage()
  os.exit(1)
end

operation=arg[1]
if operation == "usbspec" then
  doUSBSpec()
elseif operation == "hotplug" and #arg >= 3 then
-- Check if ubus is running (silences errors during bootup)
  local ubusrunning = isUbusRunning()
  if (ubusrunning) then
    doHotplug()
  end
else
  printUsage()
  os.exit(1)
end
