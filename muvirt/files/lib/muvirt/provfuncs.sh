#!/bin/sh

critical_error() {
	vmname="${1}"
	error_message="${2}"
	uci -q get "virt.${vmname}" && uci set "virt.${vmname}._downloading=-1"
	logger -t "muvirt-provision" -s "[${vmname}] ${error_message}"
	exit 1
}

deferred_error() {
	uci -q get "virt.${1}" && uci set "virt.${1}._downloading=-1"
	logger -t "muvirt-provision" -s "[${vmname}] ${2}"
}

download_imagefile() {
	vmname="${1}"
	imageurl="${2}"
	destination="${3}"
	checksum="${4}"
	logger -t "muvirt-provision" -s "[${vmname}] downloading ${imageurl} to ${destination}"
	curl -f -L "${imageurl}" -o "${destination}" || critical_error "${vmname}" "Failed to download image" && return 1
	(hashutil "${checksum}" "${destination}" > /dev/null) || critical_error "${vmname}" "Error verifying image"
	return 0
}

get_library_file_for_imageurl() {
	vmname="${1}"
	url="${2}"
	checksum="${3}"
	default_dest="${4}"
	# Format: appliance-store-id-version|url
	lookup=$(echo "${url}" | awk -F '|' '{print $1}')

	# No lookup id specified, pass through
	if [ "${url}" = "${lookup}" ]; then
		download_imagefile "${vmname}" "${url}" "${default_dest}"
		echo "${default_dest}"
		return 0
	fi

	url=$(echo "${url}" | awk -F '|' '{print $2}')
	scratch_dir=$(uci get virt.@muvirt[0].scratch)
	library_dir="${scratch_dir}/imglibrary/"

	library_file_name="${library_dir}/${lookup}.img"
	if [ -f "${library_file_name}" ]; then
		sha256sum_pass=0
		(hashutil "${checksum}" "${library_file_name}" > /dev/null) && sha256sum_pass=1
		if [ "${sha256sum_pass}" = "1" ]; then
			echo "${library_file_name}"
			logger -t "muvirt-provision" -s "[${vmname}] using existing library file ${library_file_name}"
			return 0
		else
			logger -t "muvirt-provision" -s "[${vmname}] hash for existing library file ${library_file_name} does not match expected ${checksum}, removing"
			# Remove the existing file and download again below
			rm "${library_file_name}"
		fi
	fi
	download_imagefile "${vmname}" "${url}" "${library_file_name}" "${sha256sum}"
	echo "${library_file_name}"
	return 0
}

start_and_wait_for_vm_to_exit() {
	vmname="${1}"
	/etc/init.d/muvirt start "${vmname}"

	sleep 5
	timeout=3600
	start_time=$(date +%s)

	while true; do
		RUNNING_STATUS=$(ubus call service list '{"name":"muvirt"}' | jsonfilter -e "@.muvirt.instances.${vmname}.running")

		cur_time=$(date +%s)
		elapsed_time=$((cur_time - start_time))

		if [ -z "${RUNNING_STATUS}" ]; then
			deferred_error "${vmname}" "First time boot failed to launch qemu (possibly missing storage/files), check system log for details"
			return 1
		fi

		if [ "${RUNNING_STATUS}" = "false" ]; then
			TERM_CODE=$(ubus call service list '{"name":"muvirt"}' | jsonfilter -e "@.muvirt.instances.${vmname}.exit_code")
			if [ "${TERM_CODE}" = "0" ]; then
				# Ubuntu 'probes' the hardware (GFX and initrd mode) by checking to see if the kernel
				# panic'ed and restarted on it's first run.
				# If the firstboot exits too quickly then try again
				if [ "${elapsed_time}" -lt 60 ]; then
					logger -t "muvirt-provision" "${vmname} first boot finished in less than 60s (${elapsed_time}), probably Ubuntu. Trying again"
					/etc/init.d/muvirt start "${vmname}"
					continue
				else
					break
				fi
			else
				deferred_error "${vmname}" "First time boot failed to start qemu, check system log for details"
				return 1
			fi
		fi

		if [ "${elapsed_time}" -gt ${timeout} ]; then
			/etc/init.d/muvirt stop "${vmname}"
			deferred_error "${vmname}" "First boot timed out - does this VM have cloud-init?"
		fi

		sleep 10
	done
	return 0
}

get_qcow2_image_size() {
	imagefile="${1}"
	VIRTUAL_SIZE=$(qemu-img info --output=json "${imagefile}" | jsonfilter -e '@["virtual-size"]')
	MB_SIZE=$(( VIRTUAL_SIZE / 1024 / 1024 ))
	echo "${VIRTUAL_SIZE}"
}

get_random_disk_serial() {
	newdisksn=$(lua -e 'local muvirt = require "muvirt"; print(muvirt.generate_disk_serial())')
	echo "${newdisksn}"
}