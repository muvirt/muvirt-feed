#!/usr/bin/env lua
plutils = require 'pl.utils'
plstringx = require "pl.stringx"
hashutil = {}

function hashutil.do_file_hash(hashname, filepath)
    local cmdline = "openssl dgst -" .. hashname .. " " .. plutils.quote_arg(filepath)
    local hash_success, hash_return, hash_stdout, hash_stderr =
        plutils.executeex(cmdline)

    if (hash_success ~= true) then
        error("Failed to get hash for file: " .. hash_stderr)
    end

    location_of_equals = hash_stdout:find("=")
    if (location_of_equals == nil) then
        error("Could not parse output: " .. hash_stdout)
    end

    local hash = hash_stdout:sub(location_of_equals+2)
    hash = plstringx.strip(hash)
    return hash
end

--- Verify a file against a string of format "<hashtype>:<hash>"
-- @param The hash string in format "<hashtype>:<hash>"
-- @param The file to verify
function hashutil.verify_hash_string(hashstring, filepath)
    local location_of_colon = hashstring:find(":")
    if (location_of_colon == nil) then
        error("Could not parse input hash string, no \":\" found")
    end

    hash_type = hashstring:sub(0, location_of_colon-1)
    hash = hashstring:sub(location_of_colon+1)

    local our_hash = hashutil.do_file_hash(hash_type, filepath)
    local hash_match = our_hash == hash
    return hash_match,our_hash
end

return hashutil
