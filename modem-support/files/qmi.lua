require "pl"
local jsonc = require 'luci.jsonc'

local qmi = {}

local DMS_MODEL_INFO_CACHE_FILE = "/var/tmp/qmi/model_info.txt"
local DMS_IDS_CACHE_FILE = "/var/tmp/qmi/dms_ids.txt"
local UIM_IMSI_CACHE_FILE = "/var/tmp/qmi/uim_cache.txt"

function parse_nas_signal_info(qmicli_output)
    local return_values = {}
    local lines = stringx.splitlines(qmicli_output)
    curmode = nil
    for i,v in ipairs(lines) do
        if (v:find("LTE") ~= nil) then
            curmode = "lte"
        elseif (v:find("5G") ~= nil) then
            curmode = "nr"
        else
            local measurement, value = v:match("(%w+): '(-*[%d%p]+)")
            if (curmode ~= nil and value ~= nil) then
                local return_key = curmode .. "_" .. measurement:lower()
                return_values[return_key] = tonumber(value)
            end
        end
    end
    return return_values
end

qmi.parse_nas_signal_info = parse_nas_signal_info


function parse_nas_serving_system(qmicli_output)
    local return_values = {}
    local lines = stringx.splitlines(qmicli_output)
    for i,v in ipairs(lines) do
        local lastchar = v:sub(#v,#v)
        local key, value = v:match("[\t ]+([%w ]+): '(-*[%w%d ]+)")
        if (key == "Registration state") then
            return_values["state"] = value
        elseif (key == "LTE tracking area code") then
            return_values["tac"] = value
        elseif (key == "3GPP cell ID") then
            return_values["cell_id"] = value
        elseif (key == "MCC") or (key == "MNC") or (key == "CS") or (key == "PS") then
            local lower_key = key:lower()
            return_values[lower_key] = value
        elseif (key == "Description") then
            return_values["network_name"] = value
        end
    end
    return return_values
end

qmi.parse_nas_serving_system = parse_nas_serving_system


function parse_tx_rx_info(qmicli_output)
    local return_values = {}
    local lines = stringx.splitlines(qmicli_output)
    curchain = nil
    for i,v in ipairs(lines) do
        local firstchar = v:sub(1,1)
        if (firstchar == 'R') then
            curchain_num = v:match('RX Chain (%d):')
            curchain = "rx_" .. curchain_num .. "_"
        elseif (firstchar == 'T') then
            curchain = "tx_"
        else
            local measurement, value = v:match("[\t ]+([%w ]+): '(-*[%w%d ]+)")
            if (value ~= nil) then
                local return_key = nil
                local return_value = nil
                if (measurement:find("tuned") ~= nil) then
                    return_key = curchain .. "tuned"
                    return_value = tostring(value:find("yes") ~= nil)
                elseif (measurement:find("traffic") ~= nil) then
                    return_key = curchain .. "traffic"
                    return_value = tostring(value:find("yes") ~= nil)
                else
                    return_key = curchain .. measurement:lower()
                    return_value = value
                end
                return_values[return_key] = return_value
            end
        end
    end
    return return_values
end

qmi.parse_tx_rx_info = parse_tx_rx_info

function parse_nas_system_info(qmicli_output)
    local return_values = {}
    local lines = stringx.splitlines(qmicli_output)
    local cur_service = nil
    local service_name = nil
    this_service_table = {}
    for i,v in ipairs(lines) do
        local firstchar = v:sub(1,1)
        local lastchar = v:sub(#v,#v)
        if (firstchar == '[') then
            -- do nothing
        elseif (lastchar == ':') then
            if (cur_service ~= nil) then
                return_values[service_name] = this_service_table
            end
            cur_service = v:match("[\t ]+([%d%a-\(\) ]+) service:")
            service_name = cur_service:gsub(" ","-"):lower()
            this_service_table = {}
        else
            local key, value = v:match("[\t ]+([%w ]+): '(-*[%w%d ]+)")
            if (value ~= nil) then
                local flat_key = key:lower():gsub(" ","_")
                this_service_table[flat_key] = value
            end
        end
    end
    return_values[service_name] = this_service_table
    return return_values
end

qmi.parse_nas_system_info = parse_nas_system_info

function parse_dms_get_ids(dms_get_ids_output)
    local return_values = {}
    local lines = stringx.splitlines(dms_get_ids_output)
    for i,v in ipairs(lines) do
        local firstchar = v:sub(1,1)
        if (firstchar == '[') then
            -- do nothing
        else
            key,value = v:match("[\t ]+([%w ]+): '([%w%d]+)'")
            if (key == "ESN") or (key == "MEID") then
                -- Are these dead yet?
            elseif (value ~= nil) then
                if (key == "IMEI") then
                    return_values["imei"] = value
                elseif (key == "IMEI SV") then
                    return_values["sv"] = value
                end
            end
        end
    end
    return return_values
end

function parse_squashed_model_info(squashed_lines)
    local return_values = {}
    local lines = stringx.splitlines(squashed_lines)
    for i,v in ipairs(lines) do
        local lastchar = v:sub(#v,#v)
        if (lastchar == "'") then
            key,value = v:match("[\t ]+([%w ]+): '([%w-]+)'")
            if (key ~= nil) and (value ~= nil) then
                local lower_key = key:lower()
                return_values[lower_key] = value
            end
        end
    end
    return return_values
end

function parse_hardware_info(dms_get_ids_output, dms_manufacturer_model_revision_squashed)
    local dms_ids = parse_dms_get_ids(dms_get_ids_output)
    local model_revision = parse_squashed_model_info(dms_manufacturer_model_revision_squashed)
    local return_keys = tablex.merge(dms_ids, model_revision, true)
    return return_keys
end

qmi.parse_hardware_info = parse_hardware_info

function get_hardware_info_cache()
    local dms_ids_text = nil
    local dms_model_info_text = nil
    local has_cache_dir = path.isdir("/var/tmp/qmi")

    if (has_cache_dir) then
        if (path.isfile(DMS_IDS_CACHE_FILE)) then
            dms_ids_text = utils.readfile("/var/tmp/qmi/dms_ids.txt",false)
        end
        if (path.isfile(DMS_MODEL_INFO_CACHE_FILE)) then
            dms_model_info_text = utils.readfile(DMS_MODEL_INFO_CACHE_FILE,false)
        end
    else
        dir.makepath("/var/tmp/qmi")
    end

    if (dms_ids_text == nil) then
        dms_ids_success, dms_ids_return, dms_ids_text, dms_ids_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p --dms-get-ids")
        if (dms_ids_success) then
            utils.writefile(DMS_IDS_CACHE_FILE, dms_ids_text, false)
        end
    end
    if (dms_model_info_text == nil) then
        local dms_commands = {}
        dms_commands[#dms_commands+1] = "--dms-get-manufacturer"
        dms_commands[#dms_commands+1] = "--dms-get-model"
        dms_commands[#dms_commands+1] = "--dms-get-revision"
        local dms_lines = ""
        local all_lines_success = true
        for k,v in ipairs(dms_commands) do
            this_line_success, this_line_return, this_line_text, this_line_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p " .. v)
            if (this_line_success == true) then
                dms_lines = dms_lines .. "\n" .. this_line_text
            else
                all_lines_success = false
            end
        end
        if (all_lines_success == true) then
            utils.writefile = utils.writefile(DMS_MODEL_INFO_CACHE_FILE, dms_lines, false)
        end
        dms_model_info_text = dms_lines
    end
    return dms_ids_text, dms_model_info_text
end

function parse_uim_imsi_response(qmicli_output)
    local r_result = qmicli_output:find("Read result:")
    if (r_result == nil) then
        return nil
    end
    local val_delim = qmicli_output:find(":", r_result)
    local val_substr = qmicli_output:sub(val_delim+1)
    local val_line_stripped = stringx.strip(val_substr)
    local val_split = stringx.split(val_line_stripped,":")
    local imsi_string = ""
    for i,val in ipairs(val_split) do
        if (i==2) then
            imsi_string = val:sub(1,1)
        elseif (i>2) then
            imsi_string = imsi_string .. val:sub(2,2) .. val:sub(1,1)
        end
    end
    return imsi_string
end

qmi.parse_uim_imsi_response = parse_uim_imsi_response

function get_sim_imsi()
    local imsi_read_success, imsi_read_turn,
        imsi_read_stdout, imsi_read_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p --uim-read-transparent=0x3F00,0x7FFF,0x6F07")
    if (imsi_read_success) then
        return parse_uim_imsi_response(imsi_read_stdout)
    end
    return nil
end

function parse_uim_slot_status(qmicli_output)
    local lines = stringx.splitlines(qmicli_output)
    local return_dict = {}
    local cur_slot = 1
    local active_slot = nil
    local cur_slot_dict = {}
    for i,v in ipairs(lines) do
        local lastchar = v:sub(#v,#v)
        local firstchar = v:sub(1,1)
        if (firstchar == '[') then
            -- do nothind
        elseif (lastchar == ':') then
            slotnum_chr = v:sub(#v-1,#v-1)
            local this_slot = tonumber(slotnum_chr)
            if (this_slot ~= cur_slot) then
                return_dict[tostring(cur_slot)] = cur_slot_dict
                cur_slot_dict = {}
                cur_slot = this_slot
            end
        else
            key,value = v:match("[\t ]+([%w ]+): ([%w-]+)")
            if (key ~= nil) and (value ~= nil) then
                if (key == "Slot status" and value == "active") then
                    active_slot = cur_slot
                end
                if (key == "Is eUICC") then
                    local is_euicc_bool = (value == "yes")
                    cur_slot_dict["is_euicc"] = is_euicc_bool
                else
                    local lower_key = key:lower():gsub(" ","_")
                    cur_slot_dict[lower_key] = value
                end             
            end
        end
    end
    return_dict[tostring(cur_slot)] = cur_slot_dict
    return_dict["active"] = active_slot
    return return_dict
end

qmi.parse_uim_slot_status = parse_uim_slot_status

function get_imsi_cached(active_slot_iccid)
    if (path.isfile(UIM_IMSI_CACHE_FILE)) then
        local imsi_cache_str = utils.readfile(UIM_IMSI_CACHE_FILE)
        local imsi_cache = pretty.read(imsi_cache_str) or {}
        cached_iccid = imsi_cache["iccid"]
        cached_imsi = imsi_cache["imsi"]
        if (active_slot_iccid == cached_iccid) and (cached_imsi ~= nil) then
            return cached_imsi
        end
    end
    local imsi = get_sim_imsi()
    if (imsi == nil) then
        return nil
    end
    local cached_details = {}
    cached_details["iccid"] = active_slot_iccid
    cached_details["imsi"] = imsi
    pretty.dump(cached_details, UIM_IMSI_CACHE_FILE)
    return imsi
end

function get_sim_info()
    local return_dict = nil
    local sim_info_success, sim_info_return,
        sim_info_stdout, sim_info_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p --uim-get-slot-status")
    if (sim_info_success) then
        return_dict = parse_uim_slot_status(sim_info_stdout)
    else
        return nil
    end
    local active_slot = tostring(return_dict["active"])
    local active_slot_iccid = nil
    if (return_dict[active_slot] ~= nil) and (return_dict[active_slot]["iccid"] ~= nil) then
        active_slot_iccid = return_dict[active_slot]["iccid"]
        local imsi = get_imsi_cached(active_slot_iccid)
        return_dict[active_slot]["imsi"] = imsi
    end
    return return_dict
end

qmi.get_sim_info = get_sim_info

function get_hardware_info()
    local dms_ids_text, dms_model_info_text = get_hardware_info_cache()
    return parse_hardware_info(dms_ids_text,  dms_model_info_text)
end

qmi.get_hardware_info = get_hardware_info

function get_signal_info()
    local signal_info_success, signal_info_return,
        signal_info_stdout, signal_info_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p --nas-get-signal-info")
    if (signal_info_success) then
        return parse_nas_signal_info(signal_info_stdout)
    end
    return {}
end

qmi.get_signal_info = get_signal_info

function get_status_overview()
    local nas_serving_system_success, nas_serving_system_return,
        nas_serving_system_stdout, nas_serving_system_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p --nas-get-serving-system")
    local nas_serving_data = {}
    if (nas_serving_system_success) then
        nas_serving_data = parse_nas_serving_system(nas_serving_system_stdout)
    end
    local nas_system_info = {}
    local nas_system_info_success, nas_system_info_return,
        nas_system_info_stdout, nas_system_info_stderr = utils.executeex("qmicli -d /dev/cdc-wdm0 -p --nas-get-system-info")
    if (nas_system_info_success) then
        nas_system_info = parse_nas_system_info(nas_system_info_stdout)
    end
    siginfo = get_signal_info()
    local bars = 0
    if (siginfo["lte_rssi"] ~= nil) then
        if (siginfo["lte_rssi"] > -65) then
            bars = 5
        elseif (siginfo["lte_rssi"] > -75) then
            bars = 4
        elseif (siginfo["lte_rssi"] > -85) then
            bars = 3
        elseif (siginfo["lte_rssi"] > -95) then
            bars = 2
        else
            bars = 1
        end
    end
    local status_overview = tablex.merge(nas_serving_data, siginfo, true)
    status_overview["bars"] = bars
    local lte_avail = false
    local nsa_avail = false
    local wcdma_avail = false
    local sa_avail = false
    local network_mode = nil
    local roaming_status = nil
    if (nas_system_info["lte"] ~= nil) then
        roaming_status = nas_system_info["lte"]["roaming_status"]
        lte_avail = (nas_system_info["lte"]["status"] == "available")
        nsa_avail = (nas_system_info["lte"]["5g_nsa_available"] == "yes") and
            (nas_system_info["lte"]["dcnr_restriction"] == "no")
    end
    if (nas_system_info["WCDMA"] ~= nil) then
        -- TODO
    end
    if (sa_avail) then
        network_mode = "5G SA"
    elseif (nsa_avail) then
        network_mode = "5G NSA / EN-DC"
    elseif (lte_avail) then
        network_mode = "4G LTE"
    elseif (wcdma_avail) then
        network_mode = "3G WCDMA"
    end

    if (roaming_status ~= nil) then
        if (roaming_status == "on") then
            status_overview["roaming"] = true
        else
            status_overview["roaming"] = false
        end
    end

    status_overview["mode"] = network_mode

    local sim_info = get_sim_info()
    if (sim_info ~= nil) and (sim_info["active"] ~= nil) then
        local active_sim_num = sim_info["active"]
        status_overview["active_sim_slot"] = active_sim_num
        local active_sim_info = sim_info[tostring(active_sim_num)]
        status_overview["active_sim_iccid"] = active_sim_info["iccid"]
        status_overview["active_sim_imsi"] = active_sim_info["imsi"]
        local other_sim_slot = 2
        if (active_sim_num == 2) then
            other_sim_slot = 1
        end
        local other_sim_slot_info = sim_info[tostring(other_sim_slot)]
        if (other_sim_slot_info ~= nil) then
            status_overview["other_sim_slot"] = other_sim_slot
            status_overview["other_sim_status"] = other_sim_slot_info["card_status"]
            status_overview["other_sim_iccid"] = other_sim_slot_info["iccid"]
        end
    end

    return status_overview
end

qmi.get_status_overview = get_status_overview

function get_mmcli_modem_info(modem_idx)
    this_modem_info_success, this_modem_info_return,
    this_modem_info_stdout, this_modem_info_stderr= utils.executeex("mmcli -m " .. tostring(modem_idx) .. " -J")
    if (this_modem_info_success ~= true) then
        return nil
    end
    local this_modem_info_json = jsonc.parse(this_modem_info_stdout)
    local this_modem_info_dict = this_modem_info_json["modem"]
    if (this_modem_info_dict == nil) or (this_modem_info_dict["generic"] == nil) then
        return nil
    end
    local generic_modem_info = this_modem_info_json["modem"]["generic"]
    local modem_info = {}
    modem_info["manufacturer"] = generic_modem_info["manufacturer"]
    modem_info["model"] = generic_modem_info["model"]
    modem_info["device"] = generic_modem_info["device"]
    return modem_info
end

function get_modems()
    local modem_info = {}
    local mmcli_modems_success, mmcli_modems_return, mmcli_modems_stdout, mmcli_modems_stderr =
        utils.executeex("mmcli -L")
    if (mmcli_modems_success) then
        local modem_lines = stringx.splitlines(mmcli_modems_stdout)
        for i,v in ipairs(modem_lines) do
            local this_modem = v:match("\/Modem\/(%d)")
            local this_modem_info = get_mmcli_modem_info(this_modem)
            if (this_modem_info ~= nil) then
                modem_info[#modem_info+1] = this_modem_info
            end
        end
    end
    local cached_modem_success, cached_modem_return, cached_modem_stdout, cached_modem_stderr =
        utils.executeex("fw_printenv modem_model")
    if (cached_modem_success) then
        local cached_modem_info = stringx.split(stringx.strip(cached_modem_stdout), "=")
        local cached_modem_device = cached_modem_info[2]
        local cached_modem_mfr_model = cached_modem_info[3]
        local cached_mfr_model_split = stringx.split(cached_modem_mfr_model, " ")
        -- Check to see we don't have this modem already
        local has_modem = false
        for i,v in ipairs(modem_info) do
            if (v["device"] == cached_modem_device) then
                has_modem = true
            end
        end
        if (has_modem ~= true) then
            local cached_modem_info = {}
            cached_modem_info["device"] = cached_modem_device
            cached_modem_info["manufacturer"] = cached_mfr_model_split[1]
            cached_modem_info["model"] = cached_mfr_model_split[2]
            modem_info[#modem_info+1] = cached_modem_info
        end
    end
    return modem_info
end

qmi.get_modems = get_modems

return qmi