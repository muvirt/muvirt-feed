#!/usr/bin/env lua

local qmi = require 'qmi'
local jsonc = require 'luci.jsonc'
local operations = {}
operations["overview"] = {}
operations["hwinfo"] = {}
operations["modems"] = {}

local op = arg[1]
if (op == "list") then
    local op_json = jsonc.stringify(operations)
    op_json = op_json:gsub("%[%]","{}")
    print(op_json)
elseif (op == "call") then
    local call_op = arg[2]
    if (call_op == nil) then
        os.exit(1)
    end
    local json_str = io.read("*a")
    local parsed_call = jsonc.parse(json_str)
    local return_data = {}
    if (call_op == "overview") then
        return_data = qmi.get_status_overview()
    elseif (call_op == "hwinfo") then
        return_data = qmi.get_hardware_info()
    elseif (call_op == "modems") then
        local modems_array = qmi.get_modems()
        return_data["modems"] = modems_array
    end
    local return_json = jsonc.stringify(return_data)
    print(return_json)
end